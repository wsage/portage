# Copyright 1999-2012 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2
# $Header: /var/cvsroot/gentoo-x86/net-p2p/gnunet/gnunet-0.8.1.ebuild,v 1.2 2010/06/20 13:56:46 xarthisius Exp $

EAPI=2

inherit autotools eutils flag-o-matic

MY_PV=${PV/_/}
MY_P="${PN}-${MY_PV}"
S="${WORKDIR}/${MY_P}"
DESCRIPTION="GNUnet is a framework for secure peer-to-peer networking."
HOMEPAGE="http://gnunet.org/"
SRC_URI="ftp://ftp.gnu.org/gnu/gnunet/${MY_P}.tar.gz"
RESTRICT="test"

IUSE="mysql nls +sqlite http esmtp X experimental"
KEYWORDS="~amd64 ~x86 ~arm"
LICENSE="GPL-3"
SLOT="0"

DEPEND=">=dev-libs/libgcrypt-1.4.5
	>=media-libs/libextractor-0.6.0
	>=dev-libs/libunistring-0.9.1.1
	>=dev-libs/gmp-4.3.0
	sys-libs/zlib
	>=net-misc/curl-7.21.0
	sys-apps/sed
	sys-libs/ncurses
	http? ( >=net-libs/libmicrohttpd-0.9.18 )
	esmtp? ( net-libs/libesmtp )
	mysql? ( >=virtual/mysql-5.0 )
	sqlite? ( >=dev-db/sqlite-3.6.0 )
	nls? ( sys-devel/gettext )
	X? (
		x11-libs/libXt
		x11-libs/libXext
		x11-libs/libX11
		x11-libs/libXrandr
	)
	"

pkg_setup() {
	enewgroup gnunetdns
	enewgroup gnunet
	enewuser gnunet -1 -1 /dev/null gnunet
	if ! use mysql && ! use sqlite; then
		einfo
		einfo "You need to specify at least one of 'mysql' or 'sqlite'"
		einfo "USE flag in order to have properly installed gnunet"
		einfo
		die "Invalid USE flag set"
	fi
}

src_unpack() {
	unpack ${A}
	sed -i -e 's|$(bindir)/gnunet|$(DESTDIR)$(bindir)/gnunet|g' \
		"${S}"/src/nat/Makefile.in || die "sed failed"
	sed -i -e 's|$(bindir)/gnunet|$(DESTDIR)$(bindir)/gnunet|g' \
		"${S}"/src/transport/Makefile.in || die "sed failed"
	sed -i -e 's|$(bindir)/gnunet|$(DESTDIR)$(bindir)/gnunet|g' \
		"${S}"/src/vpn/Makefile.in || die "sed failed"
	sed -i -e 's|$(bindir)/gnunet|$(DESTDIR)$(bindir)/gnunet|g' \
		"${S}"/src/dns/Makefile.in || die "sed failed"
	sed -i -e 's|$(bindir)/gnunet|$(DESTDIR)$(bindir)/gnunet|g' \
		"${S}"/src/exit/Makefile.in || die "sed failed"
}

src_configure() {
	append-ldflags $(no-as-needed)
	econf \
		$(use_with X x) \
		$(use_enable nls) \
		$(use_enable experimental) \
		|| die "econf failed"
}

src_compile() {
	emake -j1 || die "make failed"
}

src_install() {
	emake -j1 DESTDIR="${D}" install || die "make install failed"
	dodoc ABOUT-NLS AUTHORS INSTALL NEWS README || die
	insinto /etc
	newins "${FILESDIR}"/${P}.conf gnunet.conf || die "newins failed"
	newconfd "${FILESDIR}"/${P}.confd gnunet || die "newconfd failed"
	newinitd "${FILESDIR}"/${P}.initd gnunet || die "newinitd failed"
	keepdir /var/{lib,log}/gnunet
	fowners gnunet:gnunet /var/lib/gnunet /var/log/gnunet
}

pkg_postinst() {
	einfo
	einfo "To configure"
	einfo "	 1) Add user(s) to the gnunet group"
	einfo "	 2) Edit the server config file '/etc/gnunet.conf'"
	einfo
}
