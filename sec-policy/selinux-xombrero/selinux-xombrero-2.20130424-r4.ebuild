# Copyright 1999-2014 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2
# $Header: $
EAPI=4

IUSE=""
MODS="xombrero"
POLICY_FILES="xombrero.te xombrero.fc xombrero.if"

inherit selinux-policy-2

DESCRIPTION="SELinux policy for xombrero"

KEYWORDS="~amd64 ~x86"
